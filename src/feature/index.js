export { default as Home } from './home/Home'
export { default as About } from './about/About'
export { default as Shop } from './shop/Shop'