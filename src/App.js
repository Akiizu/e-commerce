import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom'

import { Home, About, Shop } from './feature'
import { Header, Footer } from './component'

import './App.css';

class App extends Component {
  render() {
    return (
      <div className="app-container">
        <Header />
        <div className="app-content">
          <Switch>
            <Route exact path='/' component={Home} />
            <Route exact path='/home' component={Home} />
            <Route exact path='/about' component={About} />
            <Route exact path='/shop' component={Shop} />
          </Switch>
        </div>
        <Footer />
      </div>
    );
  }
}

export default App;
