import React from 'react';
import { NavLink } from 'react-router-dom'

import { Button, Icon } from '../../component'
import './header.css'

const Header = () => {
  return (
    <header>
      <div className="app-menu-section">
        <div className="app-menu-logo"> LOGO </div>
        <div className="menu-list">
          <Button isLink>
            <NavLink to='/home' activeClassName='active-menu'>Home</NavLink>
          </Button>
          <Button isLink>
            <NavLink to='/about' activeClassName='active-menu'>About</NavLink>
          </Button>
          <Button isLink>
            <NavLink to='/shop' activeClassName='active-menu'>Shopping</NavLink>
          </Button>
        </div>
        <div className="app-menu-submenu">
          <Button

            materialProps={{
              outlined: true,
              icon: <Icon>shopping_cart</Icon>
            }}></Button>

        </div>
      </div>
    </header >
  );
}

export default Header; 