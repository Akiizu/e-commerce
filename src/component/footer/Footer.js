import React from 'react';

import './footer.css'
import { Link } from 'react-router-dom'


const scrollToTop = () => {
  document.body.scrollTop = 0; // For Safari
  document.documentElement.scrollTop = 0; // F
}


const Footer = () => {
  return (
    <footer>
      <div className='app-footer-section'>
        <div className='footer-menu'>
          <Link onClick={scrollToTop} to='/home'>Home</Link>
          <Link onClick={scrollToTop} to='/about'>About</Link>
          <Link onClick={scrollToTop} to='/shop'>Shopping</Link>
        </div>
        <div>FOOTER</div>
        <div>FOOTER</div>
      </div>
      <div className='copy-right'> Copyright © 2017 by akiizudev.com All Rights Reserved. </div>
    </footer>
  );
}

export default Footer;