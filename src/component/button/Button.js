import React, { Component } from 'react';

import MaterialButton from '@material/react-button/dist';
import './button.css';

class Button extends Component {
  state = {}

  static defaultProps = {
    onClick() { },
    materialProps: {},
  }

  render() {
    const {
      onClick,
      materialProps,
      label,
      children,
      isLink
    } = this.props
    return (
      <MaterialButton
        className={`app-button ${isLink && 'no-padding'}`}
        onClick={onClick}
        {...materialProps}
      >
        {label || children}
      </MaterialButton>
    );
  }
}

export default Button;